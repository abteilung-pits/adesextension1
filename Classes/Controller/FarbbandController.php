<?php
namespace AdesExtension\Adesextension1\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * FarbbandController
 */
class FarbbandController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * farbbandRepository
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Repository\FarbbandRepository
	 * @inject
	 */
	protected $farbbandRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {

		$selected = $this->settings["flexform"]["farbband"];

		$uniqueColors = $this->farbbandRepository->findUniqueColor($selected);
		$uniqueColorsFix = $this->farbbandRepository->findUniqueColorName($uniqueColors);
		$uniqueGroesse = $this->farbbandRepository->findUniqueGroesse($selected);
		$uniqueGroesseFix = $this->farbbandRepository->findUniqueGroesseWert($uniqueGroesse);
		$uniqueMaterial = $this->farbbandRepository->findUniqueMaterial($selected);
		$uniqueMaterialFix = $this->farbbandRepository->findUniqueMaterialName($uniqueMaterial);
		$showArtNoField 	= $this->settings["flexform"]["showArtNoField"];
		
		$this->view->assign('uniquecolors', $uniqueColorsFix);
		$this->view->assign('uniquegroesse', $uniqueGroesseFix);
		$this->view->assign('uniquematerial', $uniqueMaterialFix);
		$this->view->assign('zusatzinfo', $this->settings['flexform']['zusatzinfo']);
		$this->view->assign('showArtNoField', $showArtNoField);
		//$test = $uniqueColors[$_POST['tx_adesextension1_adesplugin1']['Filtercolor']];
		$postVars =  $_POST ;
		if (isset($_POST['farbband']['Filtercolor'])){	
			$farbe = $uniqueColors[$_POST['farbband']['Filtercolor']];
		}
		else{
			$farbe = "";
		}	
		if (isset($_POST['farbband']['Filtermaterial'])){	
			$material = $uniqueMaterial[$_POST['farbband']['Filtermaterial']];
		}
		else{
			$material = "";
		}	
		if (isset($_POST['farbband']['Filtergroesse'])){	
			$groesse = $uniqueGroesse[$_POST['farbband']['Filtergroesse']];
		}
		else{
			$groesse = "";
		}
		$artikelnummer = isset($_POST['farbband']['artikelnummer']) ? $_POST['farbband']['artikelnummer'] : "";

		$farbbands = $this->farbbandRepository->findSelectedFarbband($selected, $farbe, $material, $groesse, $artikelnummer);
		$this->view->assign('farbbands', $farbbands);
		$this->view->assign('savedvalues', $postVars);
		$this->view->assign('test', $selected);
	}

}