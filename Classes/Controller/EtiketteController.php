<?php
namespace AdesExtension\Adesextension1\Controller;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * EtiketteController
 */
class EtiketteController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * etiketteRepository
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Repository\EtiketteRepository
	 * @inject
	 */
	protected $etiketteRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
 
		
		$selected 			= $this->settings["flexform"]["etikette"];
		$uniqueColors 		= $this->etiketteRepository->findUniqueColor($selected);
		$uniqueColorsFix 	= $this->etiketteRepository->findUniqueColorName($uniqueColors);
		$uniqueDruck 		= $this->etiketteRepository->findUniqueDruck($selected);
		$uniqueDruckFix 	= $this->etiketteRepository->findUniqueDruckName($uniqueDruck);
		$uniqueGroesse 		= $this->etiketteRepository->findUniqueGroesse($selected);
		$uniqueGroesseFix 	= $this->etiketteRepository->findUniqueGroesseWert($uniqueGroesse);
		$uniqueMaterial 	= $this->etiketteRepository->findUniqueMaterial($selected);
		$uniqueMaterialFix 	= $this->etiketteRepository->findUniqueMaterialName($uniqueMaterial);
		$uniqueKlebstoff 	= $this->etiketteRepository->findUniqueKlebstoff($selected);
		$uniqueKlebstoffFix = $this->etiketteRepository->findUniqueKlebstoffName($uniqueKlebstoff);
		$showArtNoField 	= $this->settings["flexform"]["showArtNoField"];
		
		
		
		foreach ( $uniqueGroesseFix as $value ){
			$diemension = explode("x", $value );
			$key = trim(str_replace(array("mm","lfm"),"",$diemension[0]));
			$key2 = trim(str_replace(array("mm","lfm","m"),"",$diemension[1]));
			$key3 = trim(str_replace(array("mm","lfm","m"),"",$diemension[2]));
			$uniqueGroesseFixSort[$key][] = $value;
		}
		
 

		if(!empty($uniqueGroesseFixSort)){
			ksort($uniqueGroesseFixSort);
		}else{
			$uniqueGroesseFixSort = array();
		}

		
		foreach ($uniqueGroesseFixSort as $Key => $uniqueGroesseInd ){
			foreach ($uniqueGroesseInd as $indKeys => $singleValues ){
				$diemension = explode("x", $singleValues );
				$commonitems = array("mm","lfm","m","ø","breit");
				
				switch ( count($diemension) ){
					case 1:
						$key 	= trim(str_replace($commonitems,"",$diemension[0])) ;
						$key2 	= trim(str_replace($commonitems,"",$diemension[0])) ;
						break;
					case 2:
						$key	= trim(str_replace($commonitems,"",$diemension[0]));
						$key2 	= trim(str_replace($commonitems,"",$diemension[1]));
						break;
					case 3:
						$key	= 0;
						$key2 	= trim(str_replace($commonitems,"",$diemension[0])).trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[1]));
						break;
				}
				$tosortCollection[$key][$key2][] = $singleValues;
			}
		}
	 
		if(!empty($tosortCollection)){
			ksort($tosortCollection);
		}else{
			$tosortCollection = array();
		}

		foreach ($tosortCollection as $key => $value ){
			ksort($value );
			foreach ($value as $key2 => $newRecords ){
				foreach ($newRecords as $key3 => $newRecords3 ){
					$uniqueGroesseFix_1[] = $newRecords3;
				}
			}
		}
		
		
		$uniqueKern			= $this->etiketteRepository->findUniqueKern($selected);
		$uniqueKernFix 		= $this->etiketteRepository->findUniqueKernWert($uniqueKern);
		$uniqueDruckFix 	= $this->etiketteRepository->findUniqueDruckName(array($this->settings['flexform']['druckverfahren']));
		
		$this->view->assign('uniquecolors', $uniqueColorsFix);
		$this->view->assign('uniquedruck', $uniqueDruckFix);
		$this->view->assign('uniquegroesse', $uniqueGroesseFix_1);
		$this->view->assign('uniquematerial', $uniqueMaterialFix);
		$this->view->assign('uniqueklebstoff', $uniqueKlebstoffFix);
		$this->view->assign('uniquekern', $uniqueKernFix);
		$this->view->assign('zusatzinfo', $this->settings['flexform']['zusatzinfo']);
		$this->view->assign('showArtNoField', $showArtNoField);
		
		$postVars = $_POST;
		
		if (isset($_POST['etikette']['Filtercolor'])){	
			$farbe = $uniqueColors[$_POST['etikette']['Filtercolor']];
		}
		else{
			$farbe = "";
		}
		if (isset($_POST['etikette']['Filterdruck'])){	
			$druck = $uniqueDruck[$_POST['etikette']['Filterdruck']];
		}
		else{
			$druck = "";
		}	
		if (isset($_POST['etikette']['Filtermaterial'])){	
			$material = $uniqueMaterial[$_POST['etikette']['Filtermaterial']];
		}
		else{
			$material = "";
		}	
		if (isset($_POST['etikette']['Filtergroesse'])){
			$groesse = $uniqueGroesse[$_POST['etikette']['Filtergroesse']];
			$groesse1 = $uniqueGroesseFix_1[$_POST['etikette']['Filtergroesse']];
			$res = $this->etiketteRepository->findUniqueGrosseName( $groesse1 );
			$groesse = $res[0]['uid'];
		}
		else{
			$groesse = "";
		}
		if (isset($_POST['etikette']['Filterklebstoff'])){	
			$klebstoff = $uniqueKlebstoff[$_POST['etikette']['Filterklebstoff']];
		}
		else{
			$klebstoff = "";
		}
		
		if (isset($_POST['etikette']['Filterkern'])){
			$kern = $uniqueKern[$_POST['etikette']['Filterkern']];
		}
		else{
			$kern = "";
		}
		$artikelnummer = isset($_POST['etikette']['artikelnummer']) ? $_POST['etikette']['artikelnummer'] : "";
		
		$etikettes = $this->etiketteRepository->findSelectedEtikette($selected, $farbe, $druck, $material, $groesse, $klebstoff,$kern , $artikelnummer );
		$etikettess = array();
		
		unset($etikettes_1);
		if( !empty($etikettes) ){
			foreach ( $etikettes as $value ){
					$newKey =  $value->getGroesse();
					$diemension = explode("x", $newKey );
					
					switch ( count($diemension) ){
						case 1:
							$key 	= trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[0])) ;
							$key2 	= trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[0])) ;
							break;
						case 2:
							$key	= trim(str_replace(array("mm","lfm"),"",$diemension[0]));
							$key2 	= trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[1]));
							break;
						case 3:
							$key	= 0;
							$key2 	= trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[0])).trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[1]));
							$key2 	= 0;
							break;
					}
					
					$tosortCollection1[$key][$key2][] = $value;
				}
		}
		
		if(!empty($tosortCollection1)){
		ksort($tosortCollection1);
		foreach ($tosortCollection1 as $key => $value ){
			ksort($value);
			foreach ($value as $key2 => $newRecords ){
				foreach ($newRecords as $key3 => $newRecords3 ){
					$etikettes_1[] = $newRecords3;
				}
			}
		}
		}
		if(!empty($etikettes_1)) $etikettes_1;
		
		$this->view->assign('etikettes', $etikettes);
		$this->view->assign('etikettes_1', $etikettes_1);
		$this->view->assign('savedvalue', $postVars);
	}
	

}
