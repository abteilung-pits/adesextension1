<?php
namespace AdesExtension\Adesextension1\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ListgroupController
 */
class ListgroupController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * EtiketteRepository
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Repository\EtiketteRepository
	 * @inject
	 */
	protected $EtiketteRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		
		$selected 				= $this->settings["flexform"]["group"];
		$etikettes 				= $this->EtiketteRepository->findGroupSelectedEtikette($selected);

		
		if( !empty($etikettes)){
			foreach ( $etikettes as $value ){
				$farbbandtype 		= $value->getFarbbandtyp();
				$artikelnummer		= $value->getArtikelnummer();
				$bemerkungen		= $value->getBemerkungen();
				$kern				= $value->getKern();
				$etikettenrolle		= $value->getEtikettenrolle();
				$material			= $value->getMaterial();
				$klebstoff			= $value->getKlebstoff();
				$etikettenfarbe		= $value->getEtikettenfarbe();
				$groesse			= $value->getGroesse();

				$dontshowFarbbandtyp	 =  empty($farbbandtype) ? FALSE : TRUE;
				$dontshowArtikelnummer 	 =  empty($artikelnummer) ? FALSE : TRUE;
				$dontshowBemerkungen 	 =  empty($bemerkungen) ? FALSE : TRUE;
				$dontshowkern 	 		 =  empty($kern) ? FALSE : TRUE;
				$dontshowEtikettenrolle  =  empty($etikettenrolle) ? FALSE : TRUE;
				$dontshowMterial  		 =  empty($material) ? FALSE : TRUE;
				$dontshowKlebstoff  	 =  empty($klebstoff) ? FALSE : TRUE;
				$dontshowetikettenfarbe	 =  empty($etikettenfarbe) ? FALSE : TRUE;
				$dontshoweGroesse	 	 =  empty($groesse) ? FALSE : TRUE;
				
				$newKey =  $value->getGroesse();
				$diemension = explode("x", $newKey );
				switch ( count($diemension) ){
					case 1:
						$key 	= trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[0])) ;
						$key2 	= trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[0])) ;
						break;
					case 2:
						$key	= trim(str_replace(array("mm","lfm"),"",$diemension[0]));
						$key2 	= trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[1]));
						break;
					case 3:
						$key	= 0;
						$key2 	= trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[0])).trim(str_replace(array("mm","lfm","m","ø"),"",$diemension[1]));
						$key2 	= 0;
						break;
				}
				$tosortCollection1[$key][$key2][] = $value;
			}


			ksort($tosortCollection1);
			foreach ($tosortCollection1 as $key => $value ){
				ksort($value);
				foreach ($value as $key2 => $newRecords ){
					foreach ($newRecords as $key3 => $newRecords3 ){
						$etikettes_1[] = $newRecords3;
					}
				}
			}
		}

		
		/* var_dump($dontshowFarbbandtyp);exit; */
		$this->view->assign('etikettes', $etikettes_1);
		$this->view->assign('dontshowFarbbandtyp', $dontshowFarbbandtyp);
		$this->view->assign('dontshowArtikelnummer', $dontshowArtikelnummer);
		$this->view->assign('dontshowBemerkungen', $dontshowBemerkungen);
		$this->view->assign('dontshowkern', $dontshowkern);
		$this->view->assign('dontshowEtikettenrolle', $dontshowEtikettenrolle);
		$this->view->assign('dontshowMterial', $dontshowMterial);
		$this->view->assign('dontshowKlebstoff', $dontshowKlebstoff);
		$this->view->assign('dontshowetikettenfarbe', $dontshowetikettenfarbe);
		$this->view->assign('dontshoweGroesse', $dontshoweGroesse);
		
		
	}
}
