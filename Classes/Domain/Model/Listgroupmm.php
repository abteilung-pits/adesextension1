<?php
namespace AdesExtension\Adesextension1\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class Listgroupmm extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * uidLocal
	 *
	 * @var string
	 */
	protected $uidLocal = '';

	/**
	 * uidForeign
	 *
	 * @var string
	 */
	protected $uidForeign = '';



	/**
	 * Returns the uidLocal
	 *
	 * @return string $uidLocal
	 */
	public function getUidLocal() {
		return $this->uidLocal;
	}

	/**
	 * Sets the uidLocal
	 *
	 * @param string $uidLocal
	 * @return void
	 */
	public function setUidLocal($uidLocal) {
		$this->uidLocal = $uidLocal;
	}

	/**
	 * Returns the uidForeign
	 *
	 * @return string $uidForeign
	 */
	public function getUidLocal() {
		return $this->uidForeign;
	}

	/**
	 * Sets the uidForeign
	 *
	 * @param string $uidForeign
	 * @return void
	 */
	public function setUidLocal($uidForeign) {
		$this->uidForeign = $uidForeign;
	}

}