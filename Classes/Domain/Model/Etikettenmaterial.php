<?php
namespace AdesExtension\Adesextension1\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Etikettenmaterial
 */
class Etikettenmaterial extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * fileinfo
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 * @lazy
	 */
	protected $fileinfo;

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}


	/**
	 * Returns the fileinfo
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $fileinfo
	 */
	public function getFileinfo() {
		if (!is_object($this->fileinfo)){
			return null;
		} elseif ($this->fileinfo instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy) {
			$this->fileinfo->_loadRealInstance();
		}
		return $this->fileinfo->getOriginalResource();
	}

	/**
	 * Sets the fileinfo
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $fileinfo
	 * @return void
	 */
	public function setFileinfo($fileinfo) {
		$this->fileinfo = $fileinfo;
	}



}