<?php
namespace AdesExtension\Adesextension1\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class Etikette extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Artikelnr.
	 *
	 * @var string
	 */
	protected $artikelnummer = '';

	/**
	 * Etiketten / Rollen
	 *
	 * @var string
	 */
	protected $etikettenrolle = '';

	/**
	 * farbbandtyp
	 *
	 * @var string
	 */
	protected $farbbandtyp = '';

	/**
	 * bemerkungen
	 *
	 * @var string
	 */
	protected $bemerkungen = '';

	/**
	 * etikettenfarbe
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Model\Etikettenfarbe
	 */
	protected $etikettenfarbe = NULL;

	/**
	 * druckverfahren
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Model\Etikettendruckverfahren
	 */
	protected $druckverfahren = NULL;

	/**
	 * kern
	 *
	 * @var  \AdesExtension\Adesextension1\Domain\Model\Etikettenkern
	 */
	protected $kern = NULL;


	/**
	 * groesse
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Model\Etikettengroesse
	 */
	protected $groesse = NULL;

	/**
	 * klebstoff
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Model\Etikettenklebstoff
	 */
	protected $klebstoff = NULL;

	/**
	 * material
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Model\Etikettenmaterial
	 */
	protected $material = NULL;


	/**
	 * overlaytext
	 *
	 * @var string
	 */
	protected $overlaytext = '';


	  /**
     * overlayimage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $overlayimage = null;

     /**
     * @var string
     */
    protected $overlaylink;

     /**
     * @var string
     */
    protected $overlaylinktext;




	/**
	 * Returns the artikelnummer
	 *
	 * @return string $artikelnummer
	 */
	public function getArtikelnummer() {
		return $this->artikelnummer;
	}

	/**
	 * Sets the artikelnummer
	 *
	 * @param string $artikelnummer
	 * @return void
	 */
	public function setArtikelnummer($artikelnummer) {
		$this->artikelnummer = $artikelnummer;
	}

	/**
	 * Returns the etikettenrolle
	 *
	 * @return string $etikettenrolle
	 */
	public function getEtikettenrolle() {
		return $this->etikettenrolle;
	}

	/**
	 * Sets the etikettenrolle
	 *
	 * @param string $etikettenrolle
	 * @return void
	 */
	public function setEtikettenrolle($etikettenrolle) {
		$this->etikettenrolle = $etikettenrolle;
	}

	/**
	 * Returns the farbbandtyp
	 *
	 * @return string $farbbandtyp
	 */
	public function getFarbbandtyp() {
		return $this->farbbandtyp;
	}

	/**
	 * Sets the farbbandtyp
	 *
	 * @param string $farbbandtyp
	 * @return void
	 */
	public function setFarbbandtyp($farbbandtyp) {
		$this->farbbandtyp = $farbbandtyp;
	}

	/**
	 * Returns the bemerkungen
	 *
	 * @return string $bemerkungen
	 */
	public function getBemerkungen() {
		return $this->bemerkungen;
	}

	/**
	 * Sets the bemerkungen
	 *
	 * @param string $bemerkungen
	 * @return void
	 */
	public function setBemerkungen($bemerkungen) {
		$this->bemerkungen = $bemerkungen;
	}

	/**
	 * Returns the etikettenfarbe
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Etikettenfarbe $etikettenfarbe
	 */
	public function getEtikettenfarbe() {
		return $this->etikettenfarbe;
	}

	/**
	 * Sets the etikettenfarbe
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Etikettenfarbe $etikettenfarbe
	 * @return void
	 */
	public function setEtikettenfarbe(\AdesExtension\Adesextension1\Domain\Model\Etikettenfarbe $etikettenfarbe) {
		$this->etikettenfarbe = $etikettenfarbe;
	}

	/**
	 * Returns the druckverfahren
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Etikettendruckverfahren $druckverfahren
	 */
	public function getDruckverfahren() {
		return $this->druckverfahren;
	}

	/**
	 * Sets the druckverfahren
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Etikettendruckverfahren $druckverfahren
	 * @return void
	 */
	public function setDruckverfahren(\AdesExtension\Adesextension1\Domain\Model\Etikettendruckverfahren $druckverfahren) {
		$this->druckverfahren = $druckverfahren;
	}

	/**
	 * Returns the groesse
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Etikettengroesse $groesse
	 */
	public function getGroesse() {
		return $this->groesse;
	}

	/**
	 * Sets the groesse
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Etikettengroesse $groesse
	 * @return void
	 */
	public function setGroesse(\AdesExtension\Adesextension1\Domain\Model\Etikettengroesse $groesse) {
		$this->groesse = $groesse;
	}

	/**
	 * Returns the klebstoff
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Etikettenklebstoff $klebstoff
	 */
	public function getKlebstoff() {
		return $this->klebstoff;
	}

	/**
	 * Sets the klebstoff
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Etikettenklebstoff $klebstoff
	 * @return void
	 */
	public function setKlebstoff(\AdesExtension\Adesextension1\Domain\Model\Etikettenklebstoff $klebstoff) {
		$this->klebstoff = $klebstoff;
	}

	/**
	 * Returns the material
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Etikettenmaterial $material
	 */
	public function getMaterial() {
		return $this->material;
	}

	/**
	 * Sets the material
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Etikettenmaterial $material
	 * @return void
	 */
	public function setMaterial(\AdesExtension\Adesextension1\Domain\Model\Etikettenmaterial $material) {
		$this->material = $material;
	}


	/**
	 * Returns the groesse
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Etikettenkern $kern
	 */
	public function getKern() {
		return $this->kern;
	}

	/**
	 * Sets the groesse
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Etikettenkern $kern
	 * @return void
	 */
	public function setKern(\AdesExtension\Adesextension1\Domain\Model\Etikettenkern $kern) {
		$this->kern = $kern;
	}

	/**
	 * Returns the overlaytext
	 *
	 * @return string $overlaytext
	 */
	public function getOverlaytext() {
		return trim($this->overlaytext);
	}

	/**
	 * Sets the overlaytext
	 *
	 * @param string $overlaytext
	 * @return void
	 */
	public function setOverlaytext($overlaytext) {
		$this->overlaytext = $overlaytext;
	}


	  /**
     * Returns the overlayimage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $overlayimage
     */
    public function getOverlayimage()
    {
        return $this->overlayimage;
    }

    /**
     * Sets the overlayimage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $overlayimage
     * @return void
     */
    public function setOverlayimage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $overlayimage)
    {
        $this->overlayimage = $overlayimage;
    }


    /**
     * Set overlaylink
     *
     * @param string $overlaylink
     * @return void
     */
    public function setOverlaylink($overlaylink)
    {
        $this->overlaylink = $overlaylink;
    }

    /**
     * Set overlaylinktext
     *
     * @param string $overlaylinktext
     * @return void
     */
    public function setOverlaylinkText($overlaylinktext)
    {
        $this->overlaylinktext = $overlaylink;
    }

    /**
     * Get link
     *
     * @return mixed
     * @param void
     */
    public function getOverlaylink()
    {

        return $this->overlaylink !== null ? $this->overlaylink : '';
    }

    /**
     * Get link
     *
     * @return mixed
     * @param void
     */
    public function getOverlaylinkText()
    {

        return $this->overlaylinktext !== null ? $this->overlaylinktext : '';
    }

}
