<?php
namespace AdesExtension\Adesextension1\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class Listgroup extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Artikelnr.
	 *
	 * @var string
	 */
	protected $grouptitle = '';

	/**
	 * Etiketten / Rollen
	 *
	 * @var string
	 */
	protected $relatedarticle = '';



	/**
	 * Returns the grouptitle
	 *
	 * @return string $grouptitle
	 */
	public function getGrouptitle() {
		return $this->grouptitle;
	}

	/**
	 * Sets the grouptitle
	 *
	 * @param string $grouptitle
	 * @return void
	 */
	public function setGrouptitle($grouptitle) {
		$this->grouptitle = $grouptitle;
	}

	/**
	 * Returns the relatedarticle
	 *
	 * @return string $relatedarticle
	 */
	public function getRelatedarticle() {
		return $this->relatedarticle;
	}

	/**
	 * Sets the relatedarticle
	 *
	 * @param string $relatedarticle
	 * @return void
	 */
	public function setRelatedarticle($relatedarticle) {
		$this->relatedarticle = $relatedarticle;
	}

}