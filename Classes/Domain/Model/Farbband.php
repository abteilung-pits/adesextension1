<?php
namespace AdesExtension\Adesextension1\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Farbband
 */
class Farbband extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Artikelnr.
	 *
	 * @var string
	 */
	protected $artikelnummer = '';

	/**
	 * Rolle / Pack
	 *
	 * @var string
	 */
	protected $rollepack = '';

	/**
	 * bemerkungen
	 *
	 * @var string
	 */
	protected $bemerkungen = '';

	/**
	 * groesse
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Model\Farbbandgroesse
	 */
	protected $groesse = NULL;

	/**
	 * farbe
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Model\Farbbandfarbe
	 */
	protected $farbe = NULL;

	/**
	 * etikettenmaterial
	 *
	 * @var \AdesExtension\Adesextension1\Domain\Model\Farbbandmaterial
	 */
	protected $etikettenmaterial = NULL;

	/**
	 * Returns the artikelnummer
	 *
	 * @return string $artikelnummer
	 */
	public function getArtikelnummer() {
		return $this->artikelnummer;
	}

	/**
	 * Sets the artikelnummer
	 *
	 * @param string $artikelnummer
	 * @return void
	 */
	public function setArtikelnummer($artikelnummer) {
		$this->artikelnummer = $artikelnummer;
	}

	/**
	 * Returns the rollepack
	 *
	 * @return string $rollepack
	 */
	public function getRollepack() {
		return $this->rollepack;
	}

	/**
	 * Sets the rollepack
	 *
	 * @param string $rollepack
	 * @return void
	 */
	public function setRollepack($rollepack) {
		$this->rollepack = $rollepack;
	}

	/**
	 * Returns the bemerkungen
	 *
	 * @return string $bemerkungen
	 */
	public function getBemerkungen() {
		return $this->bemerkungen;
	}

	/**
	 * Sets the bemerkungen
	 *
	 * @param string $bemerkungen
	 * @return void
	 */
	public function setBemerkungen($bemerkungen) {
		$this->bemerkungen = $bemerkungen;
	}

	/**
	 * Returns the groesse
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Farbbandgroesse $groesse
	 */
	public function getGroesse() {
		return $this->groesse;
	}

	/**
	 * Sets the groesse
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Farbbandgroesse $groesse
	 * @return void
	 */
	public function setGroesse(\AdesExtension\Adesextension1\Domain\Model\Farbbandgroesse $groesse) {
		$this->groesse = $groesse;
	}

	/**
	 * Returns the farbe
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Farbbandfarbe $farbe
	 */
	public function getFarbe() {
		return $this->farbe;
	}

	/**
	 * Sets the farbe
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Farbbandfarbe $farbe
	 * @return void
	 */
	public function setFarbe(\AdesExtension\Adesextension1\Domain\Model\Farbbandfarbe $farbe) {
		$this->farbe = $farbe;
	}

	/**
	 * Returns the etikettenmaterial
	 *
	 * @return \AdesExtension\Adesextension1\Domain\Model\Farbbandmaterial $etikettenmaterial
	 */
	public function getEtikettenmaterial() {
		return $this->etikettenmaterial;
	}

	/**
	 * Sets the etikettenmaterial
	 *
	 * @param \AdesExtension\Adesextension1\Domain\Model\Farbbandmaterial $etikettenmaterial
	 * @return void
	 */
	public function setEtikettenmaterial(\AdesExtension\Adesextension1\Domain\Model\Farbbandmaterial $etikettenmaterial) {
		$this->etikettenmaterial = $etikettenmaterial;
	}

}