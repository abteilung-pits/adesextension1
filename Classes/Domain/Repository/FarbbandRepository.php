<?php
namespace AdesExtension\Adesextension1\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Farbbands
 */
class FarbbandRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	public function findSelectedFarbband($selectedList, $farbe, $material, $groesse, $artikelnummer){ 
		
		$GLOBALS['TYPO3_DB']->debugOutput = true;
		$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
		$query = $this->createQuery();
		//$selectedArray = explode(',', $selectedList);
		$selectedArray = $selectedList;
		if ( ($farbe != "") || ($material != "") || ($groesse != "") || ($artikelnummer != "")){
			$a = 0;
			if ($material != ""){
				$a = $a+1;
			}
			if ($farbe != ""){
				$a = $a+2;
			}
			if ($groesse != ""){
				$a = $a+4;
			}
			if ($artikelnummer != ""){
				$a = $a+8;
			}
			switch ($a) {
				case '1':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND etikettenmaterial = '.$material.'  
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
				case '2':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND farbe = '.$farbe.'  
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
				case '3':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND farbe = '.$farbe.'  
							AND etikettenmaterial = '.$material.' 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
				case '4':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND groesse = '.$groesse.' 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
				case '5':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND groesse = '.$groesse.' 
							AND etikettenmaterial = '.$material.' 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
				case '6':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND farbe = '.$farbe.'  
							AND groesse = '.$groesse.' 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
				case '7':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND farbe = '.$farbe.'  
							AND groesse = '.$groesse.' 
							AND etikettenmaterial = '.$material.'  
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;

					
				case '8':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 							
							AND  artikelnummer LIKE "%'.$artikelnummer.'%" 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
					
				case '9':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND etikettenmaterial = '.$material.' 							
							AND  artikelnummer LIKE "%'.$artikelnummer.'%" 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
					
				case '10':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 							  
							AND farbe = '.$farbe.'
							AND  artikelnummer LIKE "%'.$artikelnummer.'%" 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
					
				case '11':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND etikettenmaterial = '.$material.' 							
							AND farbe = '.$farbe.'  							 
							AND  artikelnummer LIKE "%'.$artikelnummer.'%" 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
					
				case '12':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 							
							AND groesse = '.$groesse.'
							AND  artikelnummer LIKE "%'.$artikelnummer.'%" 
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
				
				case '13':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND etikettenmaterial = '.$material.' 							
							AND groesse = '.$groesse.'
							AND  artikelnummer LIKE "%'.$artikelnummer.'%"  
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;

				case '14':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND farbe = '.$farbe.'							 
							AND groesse = '.$groesse.' 							
							AND  artikelnummer LIKE "%'.$artikelnummer.'%"  
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;
					
				case '15':
					$result = $query->statement(
						'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
							AND deleted = 0 
							AND hidden = 0 
							AND etikettenmaterial = '.$material.'
							AND farbe = '.$farbe.'  
							AND groesse = '.$groesse.' 							
							AND  artikelnummer LIKE "%'.$artikelnummer.'%"  
						ORDER BY artikelnummer 
						ASC ')->execute();
					break;								
			}
		}
		else{ 
			$query = $this->createQuery();
			$result = $query->statement(
				'SELECT * FROM tx_adesextension1_domain_model_farbband WHERE uid IN ('.$selectedArray.')  
					AND hidden = 0 
					AND deleted = 0 
				ORDER BY artikelnummer 
				ASC ')->execute();
		}	
			return $result;
	}

	public function findUniqueColor($selectedList){
		$selectedArray = explode(',', $selectedList);
		$query = $this->createQuery();
		//$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$result = $query->statement(
			'SELECT farbe FROM tx_adesextension1_domain_model_farbband WHERE deleted = 0 
				AND hidden = 0 
				AND uid IN ('.$selectedList.')  
			ORDER BY farbe 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}
	
	public function findUniqueMaterial($selectedList){
		$selectedArray = explode(',', $selectedList);
		$query = $this->createQuery();
		//$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$result = $query->statement(
			'SELECT etikettenmaterial FROM tx_adesextension1_domain_model_farbband WHERE deleted = 0 
				AND hidden = 0 
				AND uid IN ('.$selectedList.')  
			ORDER BY etikettenmaterial 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueGroesse($selectedList){
		$selectedArray = explode(',', $selectedList);
		$query = $this->createQuery();
		//$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$result = $query->statement(
			'SELECT groesse FROM tx_adesextension1_domain_model_farbband WHERE deleted = 0 
				AND hidden = 0 
				AND uid IN ('.$selectedList.')  
			ORDER BY groesse 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueColorName($selectedArray){
		$selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		//$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$result = $query->statement(
			'SELECT name FROM tx_adesextension1_domain_model_farbbandfarbe WHERE uid IN ('.$selectedArray.')  
			ORDER BY uid 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueMaterialName($selectedArray){
		$selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		//$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$result = $query->statement(
			'SELECT name FROM tx_adesextension1_domain_model_farbbandmaterial WHERE uid IN ('.$selectedArray.')  
			ORDER BY uid 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueGroesseWert($selectedArray){
		$selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		//$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		$result = $query->statement(
			'SELECT name FROM tx_adesextension1_domain_model_farbbandgroesse WHERE uid IN ('.$selectedArray.')  
			ORDER BY uid 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	function array_multi_unique($multiArray){
		/* array_unique() für multidimensionale Arrays
		 * @param    array    $multiArray = array(array(..), array(..), ..)
		 * @return   array    Array mit einmaligen Elementen
		*/
	  	$uniqueArray = array();
	  	// alle Array-Elemente durchgehen
	  	foreach($multiArray as $subArray){
	    // prüfen, ob Element bereits im Unique-Array
		    if(!in_array($subArray, $uniqueArray)){
		      // Element hinzufügen, wenn noch nicht drin
		      $uniqueArray[] = $subArray;
		    }
	  	}
	  	$uniqueArray = $this->multiArrayToSingle($uniqueArray);
	  	return $uniqueArray;
	}

	function multiArrayToSingle($multiArray){
		/*	@param 		array 	$multiArray = array(array(..), array(..), ..) nur mit 
		*	 					2-dimensionalen array's zu verwenden die EINEN Wert im 2ten array haben.
		*	@param 		array 	$singleArray Zielarray in das die Daten abgefüllt werden.
		*	@return 	array 	eindimensionales array.
		*/
		
		$multiArray = (array) $multiArray;
		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($multiArray);
		$singleArray = array();
		foreach ($multiArray as $subArray) {
			$subArray = array_values($subArray);
			$singleArray[] = $subArray[0];
		}
		return $singleArray;	
	}
	
public function findGroupSelectedFrabband($selectedList){
		$GLOBALS['TYPO3_DB']->debugOutput = true;
		$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
		//$selectedArray = explode(',', $selectedList);
		/*Updated*/		
		$query = $this->createQuery();
		$result = $query->statement(
				'SELECT tx_adesextension1_domain_model_farbband.* FROM  tx_adesextension1_domain_model_farbband
				INNER JOIN 	tx_adesextension1_domain_model_listfrabandgroup_rel_mm ON
				(tx_adesextension1_domain_model_listfrabandgroup_rel_mm.uid_foreign = tx_adesextension1_domain_model_farbband.uid)
				 WHERE tx_adesextension1_domain_model_listfrabandgroup_rel_mm.uid_local IN ('.$selectedList.')
				GROUP BY tx_adesextension1_domain_model_farbband.uid
				ORDER BY tx_adesextension1_domain_model_farbband.artikelnummer
				ASC'
			)->execute();
		/**/
		/*$query = $this->createQuery();
			$result = $query->statement(
				'SELECT tx_adesextension1_domain_model_farbband.* FROM  tx_adesextension1_domain_model_farbband
				INNER JOIN 	tx_adesextension1_domain_model_listfrabandgroup_rel_mm ON
				(tx_adesextension1_domain_model_listfrabandgroup_rel_mm.uid_foreign = tx_adesextension1_domain_model_farbband.uid)
				 WHERE tx_adesextension1_domain_model_listfrabandgroup_rel_mm.uid_local IN ('.$selectedArray.') 
				GROUP BY tx_adesextension1_domain_model_farbband.uid
				ORDER BY tx_adesextension1_domain_model_farbband.artikelnummer
				ASC'	)->execute();*/
		return $result;
	}
}