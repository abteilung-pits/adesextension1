<?php
namespace AdesExtension\Adesextension1\Domain\Repository;
 use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Etikettes
 */
class EtiketteRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	public function findSelectedEtikette($selectedList, $farbe, $druck, $material, $groesse, $klebstoff,$kern , $artikelnummer ){
		$GLOBALS['TYPO3_DB']->debugOutput = true;
		$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
		
		$selectedArray = $selectedList;
		
		$query = $this->createQuery();
		if ( ($material != "") || ($farbe != "") || ($groesse != "") || ($druck != "") || ($klebstoff != "")  || ($kern != "") || ($artikelnummer != "")  ){
			$a = 0;
			if ($material != ""){
				$a = $a+1;
			}
			if ($farbe != ""){
				$a = $a+2;
			}
			if ($groesse != ""){
				$a = $a+4;
			}
			if ($druck != ""){
				$a = $a+8;
			}
			if ($klebstoff != ""){
				$a = $a+16;
			}
			if ($kern != ""){
				$a = $a+32;
			}
			if ($artikelnummer != ""){
				$a = $a+64;
			}
			/* if($_SERVER['REMOTE_ADDR'] == "61.12.45.98" ) echo $a; */
			switch ($a) {
				case '1':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.material = '.$material.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '2':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '3':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.material = '.$material.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '4':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '5':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.material = '.$material.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '6':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.'
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '7':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '8':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC' )->execute();
					break;
				case '9':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '10':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '11':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '12':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '13':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '14':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '15':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '16':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '17':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '18':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '19':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.') 
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '20':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '21':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '22':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '23':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '24':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '25':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '26':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '27':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '28':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '29':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '30':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '31':
					$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
							AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
							AND tx_adesextension1_domain_model_etikette.druckverfahren = '.$druck.'  
							AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
			case '32':
				$result = $query->statement(
					'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
						AND tx_adesextension1_domain_model_etikette.deleted = 0 
						AND tx_adesextension1_domain_model_etikette.hidden = 0 
						AND tx_adesextension1_domain_model_etikette.kern = '.$kern.'  
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
				break;
			case '34':
				$result = $query->statement(
					'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.') 
						AND tx_adesextension1_domain_model_etikette.deleted = 0 
						AND tx_adesextension1_domain_model_etikette.hidden = 0 
						AND tx_adesextension1_domain_model_etikette.kern = '.$kern.'  
						AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.' 
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
				break;
			case '50':
				$result = $query->statement(
					'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
						AND tx_adesextension1_domain_model_etikette.deleted = 0 
						AND tx_adesextension1_domain_model_etikette.hidden = 0 
						AND tx_adesextension1_domain_model_etikette.kern = '.$kern.'  
						AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.' 
						AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
				break;
			case '54':
				$result = $query->statement(
					'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.') 
						AND tx_adesextension1_domain_model_etikette.deleted = 0 
						AND tx_adesextension1_domain_model_etikette.hidden = 0 
						AND tx_adesextension1_domain_model_etikette.kern = '.$kern.'  
						AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.' 
						AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
						AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
				break;
			case '55':
				$result = $query->statement(
					'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
						AND tx_adesextension1_domain_model_etikette.deleted = 0 
						AND tx_adesextension1_domain_model_etikette.hidden = 0 
						AND tx_adesextension1_domain_model_etikette.kern = '.$kern.'  
						AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.' 
						AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.' 
						AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
						AND tx_adesextension1_domain_model_etikette.material = '.$material.'  
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
				break;
			case '64':
				$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
							AND tx_adesextension1_domain_model_etikette.deleted = 0
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.artikelnummer LIKE "%'.$artikelnummer.'%"  
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
				break;
			case '66':
				$result = $query->statement(
				'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
						AND tx_adesextension1_domain_model_etikette.deleted = 0
						AND tx_adesextension1_domain_model_etikette.hidden = 0
						AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
						AND tx_adesextension1_domain_model_etikette.artikelnummer LIKE "%'.$artikelnummer.'%" 
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
				break;
			case '68':
				$result = $query->statement(
				'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
						AND tx_adesextension1_domain_model_etikette.deleted = 0
						AND tx_adesextension1_domain_model_etikette.hidden = 0
						AND tx_adesextension1_domain_model_etikette.artikelnummer LIKE "%'.$artikelnummer.'%" 
						AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.' 
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
				break;
			case '70':
				$result = $query->statement(
						'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.') 
							AND tx_adesextension1_domain_model_etikette.deleted = 0 
							AND tx_adesextension1_domain_model_etikette.hidden = 0
							AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.'  
							AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.'
							AND tx_adesextension1_domain_model_etikette.artikelnummer LIKE "%'.$artikelnummer.'%" 
						ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '80':
					$result = $query->statement(
					'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
						AND tx_adesextension1_domain_model_etikette.deleted = 0
						AND tx_adesextension1_domain_model_etikette.hidden = 0
						AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.'
						AND tx_adesextension1_domain_model_etikette.artikelnummer LIKE "%'.$artikelnummer.'%" 
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '84':
					$result = $query->statement(
					'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
						AND tx_adesextension1_domain_model_etikette.deleted = 0
						AND tx_adesextension1_domain_model_etikette.hidden = 0
						AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.'
						AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.'
						AND tx_adesextension1_domain_model_etikette.artikelnummer LIKE "%'.$artikelnummer.'%" 
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
				case '86':
					$result = $query->statement(
					'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				    LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				    WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
						AND tx_adesextension1_domain_model_etikette.deleted = 0
						AND tx_adesextension1_domain_model_etikette.hidden = 0
						AND tx_adesextension1_domain_model_etikette.etikettenfarbe = '.$farbe.' 
						AND tx_adesextension1_domain_model_etikette.groesse = '.$groesse.'
						AND tx_adesextension1_domain_model_etikette.klebstoff = '.$klebstoff.'
						AND tx_adesextension1_domain_model_etikette.artikelnummer LIKE "%'.$artikelnummer.'%" 
					ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC')->execute();
					break;
					
				default :
					break;
			}
		}
		else{

			$query = $this->createQuery();
			/*$result = $query->statement(
				'SELECT * FROM tx_adesextension1_domain_model_etikette WHERE uid IN ('. $selectedArray.')
				ORDER BY groesse 
				ASC')->execute();*/

				$result = $query->statement(
				'SELECT tx_adesextension1_domain_model_etikette.* FROM tx_adesextension1_domain_model_etikette
				LEFT JOIN tx_adesextension1_domain_model_etikettengroesse ON tx_adesextension1_domain_model_etikette.groesse=tx_adesextension1_domain_model_etikettengroesse.uid
				WHERE tx_adesextension1_domain_model_etikette.uid IN ('. $selectedArray.')
				ORDER BY CAST(tx_adesextension1_domain_model_etikettengroesse.groesse AS UNSIGNED), tx_adesextension1_domain_model_etikettengroesse.groesse ASC
				')->execute();
		}
		
		return $result;
	}

	public function findUniqueColor($selectedList){
 		$selectedArray = explode(',', $selectedList);
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT etikettenfarbe FROM tx_adesextension1_domain_model_etikette WHERE uid IN ('. $selectedList.') 
			ORDER BY etikettenfarbe 
			ASC ' )->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueDruck($selectedList){
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT druckverfahren FROM tx_adesextension1_domain_model_etikette WHERE uid IN ('.$selectedList.') 
			ORDER BY druckverfahren 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueKlebstoff($selectedList){
		$selectedArray = explode(',', $selectedList);
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT klebstoff FROM tx_adesextension1_domain_model_etikette WHERE uid IN ('. $selectedList .')
			ORDER BY klebstoff 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueMaterial($selectedList){
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT material FROM tx_adesextension1_domain_model_etikette WHERE uid IN ('.$selectedList.')  
			ORDER BY material 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueGroesse($selectedList){
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT groesse FROM tx_adesextension1_domain_model_etikette WHERE uid IN ('.$selectedList.') 
			ORDER BY groesse 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueColorName($selectedArray){
		$selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT name FROM tx_adesextension1_domain_model_etikettenfarbe WHERE uid IN ('.$selectedArray.') 
			ORDER BY uid 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueDruckName($selectedArray){
        $selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT name FROM tx_adesextension1_domain_model_etikettendruckverfahren WHERE uid IN ('.$selectedArray.')  
			ORDER BY uid 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueKlebstoffName($selectedArray){
		$selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT name FROM tx_adesextension1_domain_model_etikettenklebstoff WHERE uid  IN ('.$selectedArray.')  
			ORDER BY uid 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueMaterialName($selectedArray){
		$selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		$query->execute(TRUE);
		$result = $query->statement(
			'SELECT name FROM tx_adesextension1_domain_model_etikettenmaterial WHERE uid IN ('.$selectedArray . ') 
			ORDER BY uid 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueGroesseWert($selectedArray){
		$selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		$query->execute(TRUE);
		$result = $query->statement(
			'SELECT groesse FROM tx_adesextension1_domain_model_etikettengroesse WHERE uid IN ('.$selectedArray.') 
			ORDER BY groesse 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}
	
	public function findUniqueGrosseName($selectedArray){
		$query = $this->createQuery();
		$result = $query->statement(
				'SELECT uid FROM tx_adesextension1_domain_model_etikettengroesse WHERE groesse = "'.$selectedArray.'"
			ORDER BY uid
			ASC ')->execute(TRUE);
		return $result;
	}
	function array_multi_unique($multiArray){
		/* array_unique() für multidimensionale Arrays
		 * @param    array    $multiArray = array(array(..), array(..), ..)
		 * @return   array    Array mit einmaligen Elementen
		 */
		$uniqueArray = array();
		// alle Array-Elemente durchgehen
		foreach($multiArray as $subArray){
			// prüfen, ob Element bereits im Unique-Array
			if(!in_array($subArray, $uniqueArray)){
				// Element hinzufügen, wenn noch nicht drin
				$uniqueArray[] = $subArray;
			}
		}
		$uniqueArray = $this->multiArrayToSingle($uniqueArray);
		return $uniqueArray;
	}

	function multiArrayToSingle($multiArray){
		/*	@param 		array 	$multiArray = array(array(..), array(..), ..) nur mit
		 *	 					2-dimensionalen array's zu verwenden die EINEN Wert im 2ten array haben oder nur der erste Wert gebraucht wird.
		 *	@param 		array 	$singleArray Zielarray in das die Daten abgefüllt werden.
		 *	@return 	array 	eindimensionales array.
		 */
 
 		$multiArray =  (array) $multiArray;
		
		$singleArray = array();
		foreach ($multiArray as $subArray) {
			$subArray =  (array) $subArray;
			$subArray = array_values($subArray);
			$singleArray[] = $subArray[0];
		}
		return $singleArray;
	}


	public function findUniqueKern($selectedList){
		
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT kern FROM tx_adesextension1_domain_model_etikette WHERE uid IN ('.$selectedList.')
			ORDER BY kern 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}

	public function findUniqueKernWert($selectedArray){

		$selectedArray = implode(",", $selectedArray);
		$query = $this->createQuery();
		$result = $query->statement(
			'SELECT kern FROM tx_adesextension1_domain_model_etikettenkern WHERE uid IN  ('.$selectedArray.')
			ORDER BY uid 
			ASC ')->execute(TRUE);
		$result = $this->array_multi_unique($result);
		return $result;
	}
	
	
public function findGroupSelectedEtikette($selectedList){

		$GLOBALS['TYPO3_DB']->debugOutput = true;
		$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
		$selectedArray = explode(',', $selectedList);
		$query = $this->createQuery();
			$result = $query->statement(
				'SELECT tx_adesextension1_domain_model_etikette.* FROM  tx_adesextension1_domain_model_etikette
				INNER JOIN tx_adesextension1_domain_model_listgroup_rel_mm ON
				(tx_adesextension1_domain_model_listgroup_rel_mm.uid_foreign = tx_adesextension1_domain_model_etikette.uid)
				 WHERE tx_adesextension1_domain_model_listgroup_rel_mm.uid_local IN ('.$selectedList.')  
				 GROUP BY tx_adesextension1_domain_model_etikette.uid 
				ORDER BY tx_adesextension1_domain_model_etikette.artikelnummer 
				ASC ', 
			array($selectedArray)
			)->execute();
		return $result;
	}
}