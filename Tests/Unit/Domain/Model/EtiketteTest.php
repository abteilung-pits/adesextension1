<?php

namespace AdesExtension\Adesextension1\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Baumann Lamin <lamin@gmx.ch>, Abteilung für Gestaltung GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \AdesExtension\Adesextension1\Domain\Model\Etikette.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Baumann Lamin <lamin@gmx.ch>
 */
class EtiketteTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \AdesExtension\Adesextension1\Domain\Model\Etikette
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \AdesExtension\Adesextension1\Domain\Model\Etikette();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getArtikelnummerReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getArtikelnummer()
		);
	}

	/**
	 * @test
	 */
	public function setArtikelnummerForStringSetsArtikelnummer() {
		$this->subject->setArtikelnummer('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'artikelnummer',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEtikettenrolleReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getEtikettenrolle()
		);
	}

	/**
	 * @test
	 */
	public function setEtikettenrolleForStringSetsEtikettenrolle() {
		$this->subject->setEtikettenrolle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'etikettenrolle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFarbbandtypReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getFarbbandtyp()
		);
	}

	/**
	 * @test
	 */
	public function setFarbbandtypForStringSetsFarbbandtyp() {
		$this->subject->setFarbbandtyp('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'farbbandtyp',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getBemerkungenReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getBemerkungen()
		);
	}

	/**
	 * @test
	 */
	public function setBemerkungenForStringSetsBemerkungen() {
		$this->subject->setBemerkungen('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'bemerkungen',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEtikettenfarbeReturnsInitialValueForEtikettenfarbe() {
		$this->assertEquals(
			NULL,
			$this->subject->getEtikettenfarbe()
		);
	}

	/**
	 * @test
	 */
	public function setEtikettenfarbeForEtikettenfarbeSetsEtikettenfarbe() {
		$etikettenfarbeFixture = new \AdesExtension\Adesextension1\Domain\Model\Etikettenfarbe();
		$this->subject->setEtikettenfarbe($etikettenfarbeFixture);

		$this->assertAttributeEquals(
			$etikettenfarbeFixture,
			'etikettenfarbe',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDruckverfahrenReturnsInitialValueForEtikettendruckverfahren() {
		$this->assertEquals(
			NULL,
			$this->subject->getDruckverfahren()
		);
	}

	/**
	 * @test
	 */
	public function setDruckverfahrenForEtikettendruckverfahrenSetsDruckverfahren() {
		$druckverfahrenFixture = new \AdesExtension\Adesextension1\Domain\Model\Etikettendruckverfahren();
		$this->subject->setDruckverfahren($druckverfahrenFixture);

		$this->assertAttributeEquals(
			$druckverfahrenFixture,
			'druckverfahren',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getGroesseReturnsInitialValueForEtikettengroesse() {
		$this->assertEquals(
			NULL,
			$this->subject->getGroesse()
		);
	}

	/**
	 * @test
	 */
	public function setGroesseForEtikettengroesseSetsGroesse() {
		$groesseFixture = new \AdesExtension\Adesextension1\Domain\Model\Etikettengroesse();
		$this->subject->setGroesse($groesseFixture);

		$this->assertAttributeEquals(
			$groesseFixture,
			'groesse',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getKlebstoffReturnsInitialValueForEtikettenklebstoff() {
		$this->assertEquals(
			NULL,
			$this->subject->getKlebstoff()
		);
	}

	/**
	 * @test
	 */
	public function setKlebstoffForEtikettenklebstoffSetsKlebstoff() {
		$klebstoffFixture = new \AdesExtension\Adesextension1\Domain\Model\Etikettenklebstoff();
		$this->subject->setKlebstoff($klebstoffFixture);

		$this->assertAttributeEquals(
			$klebstoffFixture,
			'klebstoff',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getMaterialReturnsInitialValueForEtikettenmaterial() {
		$this->assertEquals(
			NULL,
			$this->subject->getMaterial()
		);
	}

	/**
	 * @test
	 */
	public function setMaterialForEtikettenmaterialSetsMaterial() {
		$materialFixture = new \AdesExtension\Adesextension1\Domain\Model\Etikettenmaterial();
		$this->subject->setMaterial($materialFixture);

		$this->assertAttributeEquals(
			$materialFixture,
			'material',
			$this->subject
		);
	}
}
