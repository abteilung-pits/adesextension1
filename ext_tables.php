<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'AdesExtension.' . $_EXTKEY,
	'Adesplugin1',
	'adesplugin1'
);

$pluginSignature = str_replace('_', '', $_EXTKEY) . '_adesplugin1';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_adesplugin1.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Etiketten Extension');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_farbband', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_farbband.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_farbband');
$GLOBALS['TCA']['tx_adesextension1_domain_model_farbband'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_farbband',
		'label' => 'artikelnummer',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'artikelnummer,rollepack,bemerkungen,groesse,farbe,etikettenmaterial,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Farbband.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_farbband.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_etikette', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikette.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikette');
$GLOBALS['TCA']['tx_adesextension1_domain_model_etikette'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette',
		'label' => 'artikelnummer',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'artikelnummer,etikettenrolle,farbbandtyp,bemerkungen,etikettenfarbe,druckverfahren,groesse,klebstoff,material,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Etikette.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_etikette.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_etikettendruckverfahren', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikettendruckverfahren.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikettendruckverfahren');
$GLOBALS['TCA']['tx_adesextension1_domain_model_etikettendruckverfahren'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikettendruckverfahren',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Etikettendruckverfahren.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_etikettendruckverfahren.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_etikettenmaterial', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikettenmaterial.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikettenmaterial');
$GLOBALS['TCA']['tx_adesextension1_domain_model_etikettenmaterial'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikettenmaterial',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,file_info',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Etikettenmaterial.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_etikettenmaterial.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_etikettengroesse', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikettengroesse.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikettengroesse');
$GLOBALS['TCA']['tx_adesextension1_domain_model_etikettengroesse'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikettengroesse',
		'label' => 'groesse',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'groesse,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Etikettengroesse.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_etikettengroesse.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_etikettenklebstoff', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikettenklebstoff.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikettenklebstoff');
$GLOBALS['TCA']['tx_adesextension1_domain_model_etikettenklebstoff'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikettenklebstoff',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Etikettenklebstoff.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_etikettenklebstoff.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_etikettenfarbe', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikettenfarbe.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikettenfarbe');
$GLOBALS['TCA']['tx_adesextension1_domain_model_etikettenfarbe'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikettenfarbe',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Etikettenfarbe.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_etikettenfarbe.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_farbbandmaterial', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_farbbandmaterial.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_farbbandmaterial');
$GLOBALS['TCA']['tx_adesextension1_domain_model_farbbandmaterial'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_farbbandmaterial',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Farbbandmaterial.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_farbbandmaterial.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_farbbandfarbe', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_farbbandfarbe.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_farbbandfarbe');
$GLOBALS['TCA']['tx_adesextension1_domain_model_farbbandfarbe'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_farbbandfarbe',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Farbbandfarbe.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_farbbandfarbe.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_farbbandgroesse', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_farbbandgroesse.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_farbbandgroesse');
$GLOBALS['TCA']['tx_adesextension1_domain_model_farbbandgroesse'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_farbbandgroesse',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Farbbandgroesse.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_farbbandgroesse.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_etikettenkern', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikettenkern.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikettenkern');
$GLOBALS['TCA']['tx_adesextension1_domain_model_etikettenkern'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikettenkern',
		'label' => 'kern',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'kern,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Etikettenkern.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_etikettenkern.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_listgroup', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikettengroup.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikettengroup');
$GLOBALS['TCA']['tx_adesextension1_domain_model_listgroup'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikettengroup',
		'label' => 'grouptitle',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'kern,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Etikettengroup.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_etikettengroup.gif',
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_adesextension1_domain_model_listfrabandgroup', 'EXT:adesextension1/Resources/Private/Language/locallang_csh_tx_adesextension1_domain_model_etikettengroup.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_adesextension1_domain_model_etikettengroup');
$GLOBALS['TCA']['tx_adesextension1_domain_model_listfrabandgroup'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_frabandgroup',
		'label' => 'grouptitle',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'kern,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Farbbandgroup.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_adesextension1_domain_model_farbbandgroup.gif',
	),
);
