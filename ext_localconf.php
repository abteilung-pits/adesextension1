<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'AdesExtension.' . $_EXTKEY,
	'Adesplugin1',
	array(
		'Farbband' => 'list',
		'Etikette' => 'list',
		'Listgroup' => 'list',
		'Listfrabandgroup' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Farbband' => '',
		'Etikette' => '',
		'Listgroup' => '',
		'Listfrabandgroup' => '',
		
	)
);
