<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_adesextension1_domain_model_etikette'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_adesextension1_domain_model_etikette']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, artikelnummer, medienart, etikettenrolle, farbbandtyp, bemerkungen, etikettenfarbe, druckverfahren, kern, groesse, klebstoff, material,overlaytext,overlayimage,overlaylink,overlaylinktext',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, artikelnummer, medienart, etikettenrolle, farbbandtyp, bemerkungen, etikettenfarbe, druckverfahren, kern, groesse, klebstoff, material,overlaytext,overlayimage,overlaylink,overlaylinktext, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime',
			'columnsOverrides' => array(
                'overlaytext' => array(
                    'defaultExtras' => 'richtext[]'
                )
            )
            
			),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
                ),
                'default' => 0,
				'foreign_table' => 'tx_adesextension1_domain_model_etikette',
				'foreign_table_where' => 'AND tx_adesextension1_domain_model_etikette.pid=###CURRENT_PID### AND tx_adesextension1_domain_model_etikette.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'artikelnummer' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.artikelnummer',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'medienart' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_medienart',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_adesextension1_domain_model_medienart',
				'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
			),
		),
		'etikettenrolle' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.etikettenrolle',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'farbbandtyp' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.farbbandtyp',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'bemerkungen' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.bemerkungen',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'etikettenfarbe' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.etikettenfarbe',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_adesextension1_domain_model_etikettenfarbe',
				'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
			),
		),
		'druckverfahren' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.druckverfahren',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_adesextension1_domain_model_etikettendruckverfahren',
				'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
			),
		),
		'kern' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.kern',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_adesextension1_domain_model_etikettenkern',
				'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
			)
		),
		'groesse' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.groesse',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_adesextension1_domain_model_etikettengroesse',
				'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
			),
		),
		'klebstoff' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.klebstoff',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_adesextension1_domain_model_etikettenklebstoff',
				'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
			),
		),
		'material' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.material',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_adesextension1_domain_model_etikettenmaterial',
				'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
			),
		),

		'overlaytext' => array(
            'exclude' => 1,
            'l10n_mode' => 'noCopy',            
            'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.overlaytext',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15, 
                'softref' => 'rtehtmlarea_images,typolink_tag,images,email[subst],url',               
                'wizards' => array(
                    '_PADDING' => 2,
                    'RTE' => array(
                        'notNewRecords' => 1,
                        'RTEonly' => 1,
                        'type' => 'script',
                        'title' => 'Full screen Rich Text Editing',
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_rte.gif',
                        'module' => array(
                            'name' => 'wizard_rte',
                        ),
                    ),
                ),
            ),
        ),

        'overlayimage' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.overlayimage',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'overlayimage',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
					),
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						)
					),
					'maxitems' => 1
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),

         'overlaylink' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.overlaylink',
			'config' => array(
				'type' => 'input',
				'size' => '30',
				'softref' => 'typolink',
				'wizards' => array(
					'_PADDING' => 2,
					'link' => array(
						 'type' => 'popup',
						 'title' => 'Link',
						 'icon' => 'wizard_link.gif',
						 'module' => array(
							'name' => 'wizard_element_browser',
							'urlParameters' => array(
								'mode' => 'wizard'
							) ,
						 ) ,
						 'JSopenParams' => 'height=600,width=500,status=0,menubar=0,scrollbars=1'
					)
				)
			)
		),
		'overlaylinktext' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:adesextension1/Resources/Private/Language/locallang_db.xlf:tx_adesextension1_domain_model_etikette.overlaylinktext',
			'config' => array(
				'type' => 'input',
				'size' => 255,
			),
		),
		
	),
);
